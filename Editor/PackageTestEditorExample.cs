﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace MarcoPolo.Packages.TestPackage
{
	public class PackageTestEditorExample : Editor
	{
		[MenuItem("Test Package/Test Button")]
		private static void TestButton()
		{
			Debug.Log("You pressed the test button!");
		}
	}
}
